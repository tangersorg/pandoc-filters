use pandoc::definition::{Attr, Block};
use pandoc::to_json_filter;
use std::io;

// H1's appear on a new page
fn code_blocks(block: Block) -> Block {
    match block.clone() {
        Block::CodeBlock(
            Attr {
                id: _,
                ref classes,
                attributes: _,
            },
            code,
        ) => {
            if (*classes).contains(&"results-example".to_string()) {
                Block::Div(
                    Attr {
                        ..Default::default()
                    },
                    vec![
                        Block::RawBlock("latex".into(), "\\begin{resultsbox}".into()),
                        Block::CodeBlock(
                            Attr {
                                ..Default::default()
                            },
                            code.clone(),
                        ),
                        Block::RawBlock("latex".into(), "\\end{resultsbox}".into()),
                    ],
                )
            } else {
                block
            }
        }
        _ => block,
    }
}

fn main() -> io::Result<()> {
    to_json_filter(&mut code_blocks)
}
