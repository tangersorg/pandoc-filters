use pandoc::definition::*;
use pandoc::to_json_filter;
use std::collections::VecDeque;
use std::io;

pub struct TableBlockIterator {
    current_row: Vec<String>,
    flush_row: bool,
    input: Vec<Inline>,
    index: u32,
}

impl TableBlockIterator {
    fn new(input: Vec<Inline>) -> Self {
        Self {
            current_row: vec![],
            flush_row: false,
            input,
            index: 0,
        }
    }
}

pub struct TableBlockBuilder;

impl TableBlockBuilder {
    fn build_tablebody(rows: Vec<Row>) -> Option<TableBody> {
        rows.first().map(|first_row| {
            TableBody(
                Attr::default(),
                first_row.1.len() as u32,
                rows.clone(),
                vec![],
            )
        })
    }

    fn build_cell(cell: &String) -> Cell {
        Cell(
            Attr::default(),
            Alignment::AlignDefault,
            1,
            1,
            vec![Block::Plain(vec![Inline::Str(cell.clone())])],
        )
    }

    fn build_row(row: Vec<String>) -> Row {
        Row(Attr::default(), row.iter().map(Self::build_cell).collect())
    }
}

// Gets rid of those pesky space pandoc objects
impl Iterator for TableBlockIterator {
    type Item = Vec<String>;

    fn next(&mut self) -> Option<Self::Item> {
        if <u32 as TryInto<usize>>::try_into(self.index).unwrap() <= self.input.len() {
            if self.flush_row {
                self.flush_row = false;
                self.current_row.clear();
            }
            for next_block in &self.input[self.index as usize..] {
                // For each block, build a row
                match next_block {
                    Inline::Str(value) => {
                        self.current_row.push(value.clone());
                        self.index += 1;
                    }
                    Inline::SoftBreak => {
                        self.index += 1;
                        self.flush_row = true;
                        return Some(
                            self.current_row
                                .clone()
                                .join(" ")
                                .split(',')
                                .map(|s| s.to_string())
                                .collect(),
                        );
                    }
                    _ => {
                        self.index += 1;
                    }
                }
            }
        }
        if self.current_row.is_empty() {
            None
        } else {
            self.flush_row = true;
            Some(
                self.current_row
                    .clone()
                    .join(" ")
                    .split(',')
                    .map(|s| s.to_string())
                    .collect(),
            )
        }
    }
}

// H1's appear on a new page
fn tables(block: Block) -> Block {
    match block.clone() {
        Block::Div(
            Attr {
                id: _,
                classes,
                attributes: _,
            },
            blocks,
        ) => {
            if classes.clone().contains(&"Table".to_string()) {
                if let Some(Block::Para(para_blocks)) = blocks.first() {
                    let mut rows: VecDeque<Row> = VecDeque::new();
                    for row in TableBlockIterator::new(para_blocks.clone()) {
                        rows.push_back(TableBlockBuilder::build_row(row));
                    }
                    if let Some(cols) = rows.pop_front() {
                        let body = TableBlockBuilder::build_tablebody(rows.into())
                            .unwrap_or_else(|| panic!("Error creating table!"));
                        let colspecs: Vec<ColSpec> = (0..cols.1.len())
                            .map(|_c| ColSpec(Alignment::AlignDefault, ColWidth::ColWidthDefault))
                            .collect();
                        Block::Table(
                            Attr::default(),
                            Caption(None, vec![]),
                            colspecs,
                            TableHead(Attr::default(), vec![cols.clone()]),
                            vec![body],
                            TableFoot(Attr::default(), vec![]),
                        )
                    } else {
                        block
                    }
                } else {
                    block
                }
            } else {
                block
            }
        }
        _ => block,
    }
}

fn main() -> io::Result<()> {
    to_json_filter(&mut tables)
}
