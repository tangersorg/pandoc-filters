use pandoc::definition::{Attr, Block};
use pandoc::to_json_filter;
use std::io;

fn alert_blocks(block: Block) -> Block {
    match block.clone() {
        Block::Div(
            Attr {
                id,
                classes,
                attributes,
            },
            mut blocks,
        ) => {
            let classes = classes.clone();
            if classes.contains(&"alert".to_string()) {
                blocks.insert(
                    0,
                    Block::RawBlock("latex".into(), "\\begin{alertbox}".into()),
                );
                // End
                blocks.push(Block::RawBlock("latex".into(), "\\end{alertbox}".into()));
                Block::Div(
                    Attr {
                        id,
                        classes,
                        attributes,
                    },
                    blocks,
                )
            } else if classes.contains(&"info".to_string()) {
                blocks.insert(
                    0,
                    Block::RawBlock("latex".into(), "\\begin{infobox}".into()),
                );
                // End
                blocks.push(Block::RawBlock("latex".into(), "\\end{infobox}".into()));
                Block::Div(
                    Attr {
                        id,
                        classes,
                        attributes,
                    },
                    blocks,
                )
            } else if classes.contains(&"warning".to_string()) {
                blocks.insert(
                    0,
                    Block::RawBlock("latex".into(), "\\begin{warningbox}".into()),
                );
                // End
                blocks.push(Block::RawBlock("latex".into(), "\\end{warningbox}".into()));
                Block::Div(
                    Attr {
                        id,
                        classes,
                        attributes,
                    },
                    blocks,
                )
            } else {
                block
            }
        }
        _ => block,
    }
}

fn main() -> io::Result<()> {
    to_json_filter(&mut alert_blocks)
}
