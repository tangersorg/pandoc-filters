use pandoc::definition::Pandoc;
use pandoc::to_json_filter;
use std::io;

// H1's appear on a new page
fn check_for_logo(doc: Pandoc) -> Pandoc {
    if !doc.meta.contains_key("logo") {
        eprintln!("Error! No `logo` field declared in file!");
    }
    doc
}

fn main() -> io::Result<()> {
    to_json_filter(&mut check_for_logo)
}
